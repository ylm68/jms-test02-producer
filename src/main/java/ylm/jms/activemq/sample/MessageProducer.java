package ylm.jms.activemq.sample;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import ylm.jms.activemq.sample.dto.PersonneDto;

import javax.jms.JMSException;
import javax.jms.ObjectMessage;
import javax.jms.Session;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by ylm68 on 11/12/2014.
 */
public class MessageProducer {

  private static final Logger LOGGER = LoggerFactory.getLogger(MessageProducer.class);

  public static void main(String[] args) throws ParseException, InterruptedException {

    final SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    final String[] dates = {"21/05/1987", "10/08/2003", "26/04/2000"};

    LOGGER.info("Chargement configuration spring...");
    ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
    final JmsTemplate jmsTemplate = (JmsTemplate) context.getBean("jmsTemplatePersonne");
    LOGGER.info("Chargement configuration spring OK");

    LOGGER.info("Début envois");
    for (int i = 1; i <= 10; i++) {
      final int numero = i;
      jmsTemplate.send(new MessageCreator() {
        public ObjectMessage createMessage(Session session) throws JMSException {
          Date date = null;
          try {
            date = dateFormat.parse(dates[numero%3]);
          } catch (ParseException e) {
            date = new Date();
          }
          PersonneDto personne = new PersonneDto("prenom" + numero, "nom" + numero, 10, date);
          LOGGER.info("message envoyé sur '" + jmsTemplate.getDefaultDestination().toString() + "': [personne" + numero + "]");
          ObjectMessage message = session.createObjectMessage();
          message.setObject(personne);
          return message;
        }
      });
      Thread.sleep(1000);
    }
    LOGGER.info("Fin envois");
  }

}
